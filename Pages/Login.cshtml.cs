﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;

namespace DevOps_Web_App.Pages
{
    public class LoginModel : PageModel
    {
       [BindProperty]
        public Credential Credential { get; set; }
        
        private readonly ILogger<PrivacyModel> _logger;

        public LoginModel(ILogger<PrivacyModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
            this.Credential = new Credential { Username = "admin" };
        }

        public void OnPost()
        {

        }
    }

    public class Credential
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}